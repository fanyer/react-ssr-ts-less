import React from "react"
import "./style.less"

export interface NotFound {}

export const NotFound = ({  }: NotFound) => {
  return <div className="NotFound">NotFound</div>
}
